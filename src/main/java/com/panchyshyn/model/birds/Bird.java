package com.panchyshyn.model.birds;

public abstract class Bird {

  String name;

  public Bird() {
  }

  public String getName() {
    return name;
  }

  public abstract String tellHowMove();
}
