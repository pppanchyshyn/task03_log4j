package com.panchyshyn.model.birds;

public class Eagle extends Bird {

  public Eagle() {
    name = "Eagle";
  }

  @Override
  public String tellHowMove() {
    return "I fly in the skies";
  }
}
