package com.panchyshyn.model.birds;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BirdsFarm {

  private static Logger logger = LogManager.getLogger(BirdsFarm.class);
  private List<Bird> birdsFarm;

  public BirdsFarm() {
    birdsFarm = new ArrayList<Bird>();
    birdsFarm.add(new Eagle());
    birdsFarm.add(new Chicken());
    birdsFarm.add(new Penguin());
  }

  public void tellHowYouMove() {
    logger.info("Guest of bird's farm asks birds to tell how they move: ");
    for (Bird bird : birdsFarm) {
      logger.trace(String.format("%s's turn to answer", bird.getName()));
      logger.info(String.format("%s says: %s", bird.getName(), bird.tellHowMove()));
    }
  }
}
