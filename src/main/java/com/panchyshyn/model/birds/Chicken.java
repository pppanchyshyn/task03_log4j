package com.panchyshyn.model.birds;

public class Chicken extends Bird {

  public Chicken() {
    name = "Chicken";
  }

  @Override
  public String tellHowMove() {
    return "I run around the ground";
  }
}
