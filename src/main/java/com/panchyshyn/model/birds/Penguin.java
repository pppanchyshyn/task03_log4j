package com.panchyshyn.model.birds;

public class Penguin extends Bird{
  public Penguin() {
    name = "Penguin";
  }

  @Override
  public String tellHowMove() {
    return "I swim in the water";
  }
}
