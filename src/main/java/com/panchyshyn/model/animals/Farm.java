package com.panchyshyn.model.animals;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Farm {

  private static Logger logger = LogManager.getLogger(Farm.class);
  private List<Animal> farm;

  public Farm() {
    farm = new ArrayList<Animal>();
    farm.add(new Cat());
    farm.add(new Dog());
    farm.add(new Pig());
    farm.add(new Mouse());
    logger.trace("Farm members gathered for a meeting");
  }

  public void makeCallOver() {
    logger.info("Call-over of animals: ");
    for (Animal animal : farm) {
      if (animal.isSick) {
        logger.warn(String.format("We don't hear %s", animal.getName()));
        logger.error(String.format("%s %s", animal.getName(), animal.makeSound()));
        logger.debug(String.format("Everything ok. We see %s", animal.getName()));
        logger.trace(String.format("%s is present", animal.getName()));
      } else if (animal.isDead) {
        logger.warn(String.format("We don't hear %s", animal.getName()));
        logger.error(String.format("%s is lost", animal.getName()));
        logger.fatal(String.format("Oh, no %s has been eaten and %s", animal.getName(),
            animal.makeSound()));
      } else {
        logger.info(String.format("%s says: %s", animal.getName(), animal.makeSound()));
        logger.trace(String.format("%s is present", animal.getName()));
      }
    }
  }
}
