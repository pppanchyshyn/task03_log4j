package com.panchyshyn.model.animals;

public class Dog extends Animal {

  public Dog() {
    name = "Dog";
  }

  @Override
  public String makeSound() {
    return "Woof woof";
  }
}
