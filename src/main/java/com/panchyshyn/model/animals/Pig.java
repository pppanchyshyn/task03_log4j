package com.panchyshyn.model.animals;

public class Pig extends Animal {

  public Pig() {
    name = "Pig";
    isSick = true;
  }

  @Override
  public String makeSound() {
    return "have a sore throat and can't speak";
  }
}
