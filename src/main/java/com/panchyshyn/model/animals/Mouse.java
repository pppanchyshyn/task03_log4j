package com.panchyshyn.model.animals;

public class Mouse extends Animal{
  public Mouse(){
    name = "Mouse";
    isDead = true;
  }

  @Override
  public String makeSound() {
    return "will never make sound again";
  }
}
