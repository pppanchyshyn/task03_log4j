package com.panchyshyn.model.animals;

public abstract class Animal {

  String name;
  boolean isSick;
  boolean isDead;

  Animal() {
  }

  public String getName() {
    return name;
  }

  public abstract String makeSound();
}
