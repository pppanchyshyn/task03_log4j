package com.panchyshyn.model.animals;

public class Cat extends Animal {

  public Cat() {
    name = "Cat";
  }

  @Override
  public String makeSound() {
    return "Meow meow";
  }
}
