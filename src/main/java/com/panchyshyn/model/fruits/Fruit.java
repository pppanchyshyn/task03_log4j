package com.panchyshyn.model.fruits;

public abstract class Fruit {

  String name;
  boolean usedForAlcoholProducing;

  Fruit() {
  }

  public String getName() {
    return name;
  }

  public abstract String howToEat();
}
