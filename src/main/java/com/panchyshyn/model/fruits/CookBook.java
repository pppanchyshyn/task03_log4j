package com.panchyshyn.model.fruits;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CookBook {

  private static Logger logger = LogManager.getLogger(CookBook.class);
  private List<Fruit> cookBook;

  public CookBook() {
    cookBook = new ArrayList<Fruit>();
    cookBook.add(new Peach());
    cookBook.add(new Orange());
    cookBook.add(new Apple());
    logger.trace("The cookbook is ready to be read");
  }

  public void readCookBook() {
    for (Fruit fruit : cookBook) {
      logger.trace(String.format("You open page with %s", fruit.getName()));
      logger.info(String.format("You can make %s from %s", fruit.howToEat(), fruit.getName()));
      if (fruit.usedForAlcoholProducing) {
        logger.warn(
            "WARNING!!! Excessive alcohol consumption is harmful to your health");
      }
    }
  }
}
