package com.panchyshyn.model.fruits;

public class Peach extends Fruit {

  public Peach() {
    name = "Peach";
  }

  public String howToEat() {
    return "make mash";
  }
}
