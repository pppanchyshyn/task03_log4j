package com.panchyshyn.model.fruits;

public class Apple extends Fruit {

  public Apple() {
    name = "Apple";
    usedForAlcoholProducing = true;
  }

  public String howToEat() {
    return "make apple cider";
  }
}
