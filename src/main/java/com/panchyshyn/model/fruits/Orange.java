package com.panchyshyn.model.fruits;

public class Orange extends Fruit {

  public Orange() {
    name = "Orange";
  }

  public String howToEat() {
    return "make orange juice";
  }
}
