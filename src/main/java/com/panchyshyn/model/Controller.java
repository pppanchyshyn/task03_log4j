package com.panchyshyn.model;

import com.panchyshyn.model.animals.Farm;
import com.panchyshyn.model.birds.BirdsFarm;
import com.panchyshyn.model.fruits.CookBook;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

  private static Logger logger = LogManager.getLogger(Farm.class);
  private Farm farm;
  private BirdsFarm birdsFarm;
  private CookBook cookBook;

  public Controller() {
    farm = new Farm();
    birdsFarm = new BirdsFarm();
    cookBook = new CookBook();
  }

  public void execute() {
    logger.info("Story about farm of animals:");
    logger.info("----------------------------------------------------------");
    farm.makeCallOver();
    logger.info("----------------------------------------------------------");
    logger.info("Story about farm of birds:");
    logger.info("----------------------------------------------------------:");
    birdsFarm.tellHowYouMove();
    logger.info("----------------------------------------------------------:");
    logger.info("Cookbook:");
    logger.info("----------------------------------------------------------:");
    cookBook.readCookBook();
  }
}
