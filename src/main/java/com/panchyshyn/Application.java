package com.panchyshyn;

import com.panchyshyn.model.Controller;

public class Application {

  public static void main(String[] args) {
    new Controller().execute();
  }
}
